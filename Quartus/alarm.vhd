library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alarm is
	generic(
		WIDTH : positive := 4
	);
	port(
		clk 				: in std_logic;
		rst 				: in std_logic;
		alarm_en 		: in std_logic;		
		-- current time
		hours_L 			: in std_logic_vector(WIDTH-1 downto 0);
		hours_R 			: in std_logic_vector(WIDTH-1 downto 0);		
		minutes_L 		: in std_logic_vector(WIDTH-1 downto 0);
		minutes_R 		: in std_logic_vector(WIDTH-1 downto 0);
		-- alarm time
		set_alarm_H_L 	: in std_logic_vector(WIDTH-1 downto 0);
		set_alarm_H_R 	: in std_logic_vector(WIDTH-1 downto 0);		
		set_alarm_M_L 	: in std_logic_vector(WIDTH-1 downto 0);
		set_alarm_M_R 	: in std_logic_vector(WIDTH-1 downto 0);
		-- alarm sound
		sound_alarm 	: out std_logic
	);	
end alarm;

architecture BHV of alarm is
begin
	process(clk, alarm_en)
	begin
		if(rst = '1') then
			
		elsif(rising_edge(clk) AND alarm_en = '1') then
			if(hours_L = set_alarm_H_L AND hours_R = set_alarm_H_R AND 
				minutes_L = set_alarm_M_L AND minutes_R = set_alarm_M_R) then
					sound_alarm <= '1';
			else
				sound_alarm <= '0';
			end if;
		end if;
		
		if(alarm_en = '0') then
			sound_alarm <= '0';
		end if;
	
	end process;
end BHV;