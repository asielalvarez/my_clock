library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity clk_div is
    generic(clk_in_freq  : natural := 50000000;
            clk_out_freq : natural := 1
    );
    port(
        clk_in  : in  std_logic;
		  rst     : in  std_logic;
        clk_out : out std_logic
    );
end clk_div;

architecture BHV_DIV of clk_div is
	constant ratio : integer := clk_in_freq / clk_out_freq;	
begin
	process(clk_in, rst)
		variable count : integer range 0 to ratio;
	begin
		if(rst = '1') then
			clk_out <= '0';
            count 	:= 0;            
        elsif(clk_in'event and clk_in = '1') then
        	if(count = ratio-1) then
        		clk_out <= '1';
        		count 	:= 0;        		
        	else
        		clk_out <= '0';
        		count 	:= count + 1;
        	end if;
        end if;
	end process;
end BHV_DIV;