library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_level is
	generic(
		WIDTH : positive := 4
	);
	port(
		clk50MHz : in std_logic;
		switches : in std_logic_vector(9 downto 0);
		buttons 	: in std_logic_vector(1 downto 0);
		led0    	: out std_logic_vector(6 downto 0);
		led0_dp 	: out std_logic;
		led1    	: out std_logic_vector(6 downto 0);
		led1_dp 	: out std_logic;
		led2    	: out std_logic_vector(6 downto 0);
		led2_dp 	: out std_logic;
		led3    	: out std_logic_vector(6 downto 0);
		led3_dp 	: out std_logic;
		led4    	: out std_logic_vector(6 downto 0);
		led4_dp 	: out std_logic;
		led5    	: out std_logic_vector(6 downto 0);
		led5_dp 	: out std_logic;		
		a_sound 	: out std_logic
	);
end top_level;

architecture STR of top_level is
	signal clk 				: std_logic;
	signal hours_R 		: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	signal minutes_R 		: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	signal seconds_R 		: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	signal hours_L 		: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	signal minutes_L 		: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	signal seconds_L 		: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	signal a_hours_R 		: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	signal a_minutes_R 	: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	signal a_seconds_R 	: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	signal a_hours_L 		: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	signal a_minutes_L 	: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	signal a_seconds_L 	: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	
	signal clk0 			: std_logic_vector(6 downto 0);
	signal clk1 			: std_logic_vector(6 downto 0);
	signal clk2 			: std_logic_vector(6 downto 0);
	signal clk3 			: std_logic_vector(6 downto 0);
	signal clk4 			: std_logic_vector(6 downto 0);
	signal clk5 			: std_logic_vector(6 downto 0);
	
	signal alarm0 			: std_logic_vector(6 downto 0);
	signal alarm1 			: std_logic_vector(6 downto 0);
	signal alarm2 			: std_logic_vector(6 downto 0);
	signal alarm3 			: std_logic_vector(6 downto 0);
	signal alarm4 			: std_logic_vector(6 downto 0);
	signal alarm5 			: std_logic_vector(6 downto 0);
	
	component decoder7seg
	port(
		input  : in  std_logic_vector(3 downto 0);
		output : out std_logic_vector(6 downto 0));
	end component;

	component clk_div
	port(
	  clk_in  : in  std_logic;
	  rst     : in  std_logic;
	  clk_out : out std_logic);
	end component;
		
	component clock_time
	port(
		clk 				: in std_logic;
		rst 				: in std_logic;
		-- set clock time
		set_time 		: in std_logic;
		sel_time 		: in std_logic_vector(2 downto 0);
		-- set alarm time
		alarm_en 		: in std_logic;
		set_alarm 		: in std_logic;
		-- time
		set_number 		: in std_logic_vector(3 downto 0);
		-- clock
		hours_R 			: out std_logic_vector(WIDTH-1 downto 0);
		minutes_R 		: out std_logic_vector(WIDTH-1 downto 0);
		seconds_R 		: out std_logic_vector(WIDTH-1 downto 0);
		hours_L 			: out std_logic_vector(WIDTH-1 downto 0);
		minutes_L 		: out std_logic_vector(WIDTH-1 downto 0);
		seconds_L 		: out std_logic_vector(WIDTH-1 downto 0);
		-- alarm
		a_hours_R 		: out std_logic_vector(WIDTH-1 downto 0);
		a_minutes_R 	: out std_logic_vector(WIDTH-1 downto 0);
		a_seconds_R 	: out std_logic_vector(WIDTH-1 downto 0);
		a_hours_L 		: out std_logic_vector(WIDTH-1 downto 0);
		a_minutes_L 	: out std_logic_vector(WIDTH-1 downto 0);
		a_seconds_L 	: out std_logic_vector(WIDTH-1 downto 0);
		a_sound 			: out std_logic		
	);
	end component;

begin

    U_CLK_DIV : clk_div
	  port map(
		  clk_in  => clk50MHz,
		  rst     => '0',
		  clk_out => clk
		);
			
	 U_CLOCK_TIME : clock_time
		port map(
			clk 				=> clk,
			rst 				=> '0',
			set_time		 	=> switches(9),
			sel_time			=> switches(6 downto 4),
			alarm_en 		=> switches(8),
			set_alarm 		=> switches(7),
			set_number 		=> switches(3 downto 0),
			seconds_R 		=> seconds_R,
			minutes_R 		=> minutes_R,
			hours_R 			=> hours_R,
			seconds_L 		=> seconds_L,
			minutes_L 		=> minutes_L,
			hours_L 			=> hours_L,
			a_hours_R 		=> a_hours_R,
			a_minutes_R 	=> a_minutes_R,
			a_seconds_R 	=> a_seconds_R,
			a_hours_L 		=> a_hours_L,
			a_minutes_L 	=> a_minutes_L,
			a_seconds_L 	=> a_seconds_L,
			a_sound 			=> a_sound
		);
				
	 -- map clk leds
    U_CLK5 : decoder7seg port map(
		  input => seconds_L,
        output => clk5
	 );
    U_CLK4 : decoder7seg port map(
        input  => seconds_R,
        output => clk4
	 );
    U_CLK3 : decoder7seg port map(
        input  => minutes_L,
        output => clk3
	 );
    U_CLK2 : decoder7seg port map(			
        input  => minutes_R,
        output => clk2
	 );    
    U_CLK1 : decoder7seg port map(
        input  => hours_L,
        output => clk1
	 );
    U_CLK0 : decoder7seg port map(
        input  => hours_R,
        output => clk0
	 );
	 
	  -- map alarm leds
    U_ALARM5 : decoder7seg port map(
		  input => a_seconds_L,
        output => alarm5
	 );
    U_ALARM4 : decoder7seg port map(
        input  => a_seconds_R,
        output => alarm4
	 );
    U_ALARM3 : decoder7seg port map(
        input  => a_minutes_L,
        output => alarm3
	 );
    U_ALARM2 : decoder7seg port map(			
        input  => a_minutes_R,
        output => alarm2
	 );    
    U_ALARM1 : decoder7seg port map(
        input  => a_hours_L,
        output => alarm1
	 );
    U_ALARM0 : decoder7seg port map(
        input  => a_hours_R,
        output => alarm0
	 );
		  
	 led5_dp <= '1';
    led4_dp <= '1';
    led3_dp <= '1';
    led2_dp <= '1';
    led1_dp <= '1';
    led0_dp <= '1';
	 
	 -- set_time, set_alarm
	 process(switches(9), switches(7))
	 begin
		if switches(9) = '1' then
			led0 <= clk0;
			led1 <= clk1;
			led2 <= clk2;
			led3 <= clk3;
			led4 <= clk4;
			led5 <= clk5;
		elsif switches(7) = '1' then
			led0 <= alarm0;
			led1 <= alarm1;
			led2 <= alarm2;
			led3 <= alarm3;
			led4 <= alarm4;
			led5 <= alarm5;
		else
			led0 <= clk0;
			led1 <= clk1;
			led2 <= clk2;
			led3 <= clk3;
			led4 <= clk4;
			led5 <= clk5;
		end if;
	 end process;
	 
end STR;