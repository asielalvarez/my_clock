library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clock_time_tb is
end clock_time_tb;


architecture TB of clock_time_tb is
	constant WIDTH 		: positive := 4;
	signal clk 				: std_logic := '0';
	signal rst 				: std_logic := '1';
	-- set clock time
	signal set_time		: std_logic := '0';
	signal sel_time		: std_logic_vector(2 downto 0);
	-- set alarm time
	signal alarm_en 		: std_logic := '0';
	signal set_alarm 		: std_logic := '0';
	-- time
	signal set_number 	: std_logic_vector(3 downto 0);
	-- clock
	signal hours_R 		: std_logic_vector(WIDTH-1 downto 0);
	signal minutes_R 		: std_logic_vector(WIDTH-1 downto 0);
	signal seconds_R 		: std_logic_vector(WIDTH-1 downto 0);
	signal hours_L 		: std_logic_vector(WIDTH-1 downto 0);
	signal minutes_L 		: std_logic_vector(WIDTH-1 downto 0);
	signal seconds_L 		: std_logic_vector(WIDTH-1 downto 0);
	-- alarm
	signal a_hours_R 		: std_logic_vector(WIDTH-1 downto 0);
	signal a_minutes_R 	: std_logic_vector(WIDTH-1 downto 0);
	signal a_seconds_R 	: std_logic_vector(WIDTH-1 downto 0);
	signal a_hours_L 		: std_logic_vector(WIDTH-1 downto 0);
	signal a_minutes_L 	: std_logic_vector(WIDTH-1 downto 0);
	signal a_seconds_L 	: std_logic_vector(WIDTH-1 downto 0);
	signal a_sound 		: std_logic;
begin

	U_CLOCK_TIME : entity work.clock_time
		port map(
			clk				=> clk,
			rst 				=> rst,
			set_time 	 	=> set_time,
			sel_time 		=> sel_time,			
			set_number 		=> set_number,
			alarm_en 		=> alarm_en,
			set_alarm 		=> set_alarm,
			hours_R 			=> hours_R,
			minutes_R 		=> minutes_R,
			seconds_R 		=> seconds_R,
			hours_L 			=> hours_L,
			minutes_L 		=> minutes_L,
			seconds_L 		=> seconds_L,
			a_hours_R 		=> a_hours_R,
			a_minutes_R 	=> a_minutes_R,
			a_seconds_R 	=> a_seconds_R,
			a_hours_L 		=> a_hours_L,
			a_minutes_L 	=> a_minutes_L,
			a_seconds_L 	=> a_seconds_L,
			a_sound 			=> a_sound
		);
		
	clk <= not clk after 20 ns;
	
	process
	begin
	
		rst <= '1';
		wait for 20 ns;
		
		rst <= '0';
		wait for 20 ns;
		
		set_time <= '1';
		sel_time <= "000";
		set_number <= "1111";
		wait;
	end process;
end TB;
