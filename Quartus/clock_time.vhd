library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clock_time is
	generic(
		WIDTH : positive := 4
	);
	port(
		clk 				: in std_logic;
		rst 				: in std_logic;
		-- set clock time
		set_time 		: in std_logic;
		sel_time 		: in std_logic_vector(2 downto 0);
		-- set alarm time
		alarm_en 		: in std_logic;
		set_alarm 		: in std_logic;
		-- time
		set_number 		: in std_logic_vector(3 downto 0);
		-- clock
		hours_R 			: out std_logic_vector(WIDTH-1 downto 0);
		minutes_R 		: out std_logic_vector(WIDTH-1 downto 0);
		seconds_R 		: out std_logic_vector(WIDTH-1 downto 0);
		hours_L 			: out std_logic_vector(WIDTH-1 downto 0);
		minutes_L 		: out std_logic_vector(WIDTH-1 downto 0);
		seconds_L 		: out std_logic_vector(WIDTH-1 downto 0);
		-- alarm
		a_hours_R 		: out std_logic_vector(WIDTH-1 downto 0);
		a_minutes_R 	: out std_logic_vector(WIDTH-1 downto 0);
		a_seconds_R 	: out std_logic_vector(WIDTH-1 downto 0);
		a_hours_L 		: out std_logic_vector(WIDTH-1 downto 0);
		a_minutes_L 	: out std_logic_vector(WIDTH-1 downto 0);
		a_seconds_L 	: out std_logic_vector(WIDTH-1 downto 0);
		a_sound 			: out std_logic
	);
end clock_time;

architecture BHV of clock_time is
	-- clock
	signal hrs_L 	: std_logic_vector(WIDTH-1 downto 0) := "0010";
	signal hrs_R 	: std_logic_vector(WIDTH-1 downto 0) := "0011";
	signal mins_L 	: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	signal mins_R 	: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	signal secs_L 	: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	signal secs_R 	: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	-- alarm
	signal a_hrs_L 	: std_logic_vector(WIDTH-1 downto 0) := "0010";
	signal a_hrs_R 	: std_logic_vector(WIDTH-1 downto 0) := "0011";
	signal a_mins_L 	: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	signal a_mins_R 	: std_logic_vector(WIDTH-1 downto 0) := "0001";
	signal a_secs_L 	: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	signal a_secs_R 	: std_logic_vector(WIDTH-1 downto 0) := (others => '0');
	
begin

	process(clk)
	begin
		
		if(rst = '1') then
			hours_R 			<= (others => '0');
			minutes_R 		<= (others => '0');
			seconds_R 		<= (others => '0');
			hours_L 			<= (others => '0');
			minutes_L 		<= (others => '0');
			seconds_L 		<= (others => '0');
			a_hours_R 		<= (others => '0');
			a_minutes_R 	<= (others => '0');
			a_seconds_R 	<= (others => '0');
			a_hours_L 		<= (others => '0');
			a_minutes_L 	<= (others => '0');
			a_seconds_L 	<= (others => '0');
			a_sound 			<= '0';
			
		elsif(rising_edge(clk)) then
		
			a_sound <= '0';
						
			if(set_time = '1') then			
				
				if	  (sel_time = "000") then -- Right Second
					secs_R <= set_number;
				elsif(sel_time = "001") then -- Left Second
					secs_L <= set_number;
				elsif(sel_time = "010") then -- Right Minute
					mins_R <= set_number;
				elsif(sel_time = "011") then -- Left Minute
					mins_L <= set_number;
				elsif(sel_time = "100") then -- Right Hour
					hrs_R <= set_number;
				elsif(sel_time = "101") then -- Left Hour
					hrs_L <= set_number;
				end if;
				
			else --set_time = '0'
			
				-- Set Alarm
				if(set_alarm = '1') then
					if	  (sel_time = "000") then -- Right Second						
						a_secs_R <= set_number;
					elsif(sel_time = "001") then -- Left Second						
						a_secs_L <= set_number;
					elsif(sel_time = "010") then -- Right Minute						
						a_mins_R <= set_number;
					elsif(sel_time = "011") then -- Left Minute					
						a_mins_L <= set_number;
					elsif(sel_time = "100") then -- Right Hour						
						a_hrs_R <= set_number;
					elsif(sel_time = "101") then -- Left Hour						
						a_hrs_L <= set_number;
					end if;					
					
				end if;
				
				if(alarm_en = '1' AND hrs_L = a_hrs_L AND hrs_R = a_hrs_R AND mins_R = a_mins_R AND mins_L = a_mins_L) then
					a_sound <= '1';
				end if;
				
				-- seconds
				secs_R <= std_logic_vector(unsigned(secs_R) + 1);
				
				if(secs_R = "1001") then
					secs_R <= (others => '0');
					secs_L <= std_logic_vector(unsigned(secs_L) + 1);
					
					if(secs_L = "0101") then
						secs_L <= (others => '0');
						
						-- minutes					
						mins_R <= std_logic_vector(unsigned(mins_R) + 1);
				
						if(mins_R = "1001") then
							mins_R <= (others => '0');
							mins_L <= std_logic_vector(unsigned(mins_L) + 1);
							
							if(mins_L = "0101") then
								mins_L <= (others => '0');
								
								-- hours
								hrs_R <= std_logic_vector(unsigned(hrs_R) + 1);
				
								if(hrs_R = "1001") then
									hrs_R <= (others => '0');
									hrs_L <= std_logic_vector(unsigned(hrs_L) + 1);
								end if; -- hours
								
								if(hrs_L = "0010" and hrs_R = "0011") then								
									hrs_R <= (others => '0');
									hrs_L <= (others => '0');
								end if; -- hours
								
							end if; -- minutes
						end if;
						
					end if; -- seconds
				end if;				
			
			end if; -- else
			
			-- clock
			seconds_R 	<= secs_R;
			minutes_R 	<= mins_R;
			hours_R 		<= hrs_R;
			seconds_L 	<= secs_L;
			minutes_L 	<= mins_L;
			hours_L 		<= hrs_L;
			
			-- alarm
			a_seconds_R 	<= a_secs_R;
			a_minutes_R 	<= a_mins_R;
			a_hours_R 		<= a_hrs_R;
			a_seconds_L 	<= a_secs_L;
			a_minutes_L 	<= a_mins_L;
			a_hours_L 		<= a_hrs_L;
		
		end if;-- clk_rising
		
	end process;
end BHV;